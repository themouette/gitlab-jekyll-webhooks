#!/usr/bin/env bash

REPOSITORY=$1
CLONE_URL=$2
NAMESPACE=$3

# destination directory to build doc in.
DEST="$(pwd)/repositories/${NAMESPACE}"
# where to generate static websites
BUILDS="$(pwd)/static/${NAMESPACE}"
# branch to build
BRANCH=doc


CLONE_PATH="${DEST}/${REPOSITORY}/"
BUILD_PATH="${BUILDS}/${REPOSITORY}/"

function log {
    echo -e " \033[32m*\033[0m ${1}"
}
function error {
    echo -e "\033[31m${1}\033[0m"
}


log "Will build ${REPOSITORY} in ${CLONE_PATH} as $(whoami)"

# ensure destination is created
if [ ! -e "${DEST}" ]; then
    mkdir -p "${DEST}"
fi

# clone or update
if [ -e "${CLONE_PATH}" ]; then
    rm -rf "${CLONE_PATH}"
fi
log "git clone --branch=$BRANCH --depth=50 $CLONE_URL $CLONE_PATH"
git clone --branch="$BRANCH" --depth=50 "$CLONE_URL" "$CLONE_PATH"

log "generate static site in $BUILD_PATH"
# remove old version
if [ -e "$BUILD_PATH" ]; then
    rm -rf "$BUILD_PATH"
fi
# ensure directory is present
if [ ! -e "$BUILD_PATH" ]; then
    mkdir -p "$BUILD_PATH"
fi
log "bundle exec jekyll build --source $CLONE_PATH --destination $BUILD_PATH --trace"
jekyll build --source "${CLONE_PATH}" --destination "${BUILD_PATH}" --trace
echo "$BUILD_PATH"
ls "$BUILD_PATH"
log "OK\n"
