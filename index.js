var express = require('express');
var http = require('http');
var webhook = require('./gitlab-webhook');
var app = express();
var debug = require('debug')('jekyll-webhook');

// It is possible to change default port.
var port = process.env.PORT || 3000

nsRe = /:([^:]*)\/[^\/]*.git$/
function parseNamespace(req) {
    var url = req.body.repository.url;
    var parts = url.match(nsRe)
    if (parts && parts.length) {
        return parts[1];
    }
    return false;
}

app.use(express.bodyParser());
app.use(express.methodOverride());

// the gitlab hook handler.
//
// It is possible to configure the behavior through environment
// using following variables:
//
// * TOKEN: the token to provide
// * BRANCH: default branch to build
//
// Using Hook in gitlab
//
// http://{host}:{port}/doc?token=***&branch=gh-pages
//
// Parameters are:
//
// * token: the configured token for user identification
// * branch: possible to override default build branch
app.post('/doc', function (req, res) {
    var token = process.env.TOKEN || 'cool';
    var branch = req.param('branch') || process.env.BRANCH || 'gh-pages';
    var ns = parseNamespace(req);

    // no namespace means no build
    if (!ns) {
        debug('Unable to find a namespace. Exit.');
        return res.end();
    }

    // ensure branch is a reference
    if (branch !== '*' && branch.indexOf('/') === -1) {
        branch = 'refs/heads/' + branch;
    }

    var cmd = 'bash -x '+__dirname+'/build-static.sh {{repository.name}} {{repository.url}} '+ns;

    return webhook({
        exec: cmd,
        token: token,
        branches: branch,
        ips: '*'
    })(req, res);
});

// serve static websites
app.use('/doc', express.directory(__dirname+'/static'));
http.createServer(app).listen(port);
